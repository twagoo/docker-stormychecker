= Docker image and compose setup for stormychecker
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

See the https://github.com/acdh-oeaw/stormychecker[stormychecker] project on GitHub.


== Build

The image build process builds stormycrawler from sources. Sources are retrieved from
GitHub, see `copy_data.sh`.

== Use

If you checkout this repository and run `docker-compose up -d` in the `compose` directory,
that should be enough. On first run a database volume will be created and a database 
schema will be initiated.

Data can be loaded into the database with the `run-sql.sh` script in the compose
directory:

[source,sh]
----
./run-sql.sh urls.sql && ./run-sql.sh status.sql (edited) 
----
