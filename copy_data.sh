#!/bin/bash

export STORMY_BRANCH="aba77cd8f5cae95e40006e8b4faf9119c17a215f"
export STORMY_REPO_URL="https://github.com/acdh-oeaw/stormychecker"


init_data (){
	STORMY_GIT_DIRECTORY="$(pwd)/stormy-src"
	
	cleanup_data

	mkdir -p "${STORMY_GIT_DIRECTORY}"
	(
  		TMP_TGZ=$(mktemp)
		cd "${STORMY_GIT_DIRECTORY}" && 
			wget -O "${TMP_TGZ}" "${STORMY_REPO_URL}/archive/${STORMY_BRANCH}.tar.gz" &&
			tar zxvf "${TMP_TGZ}" --strip-components 1 &&
			rm "${TMP_TGZ}"
	)
}

cleanup_data () {
    if [ -d "${STORMY_GIT_DIRECTORY}" ]; then
    	echo "Cleaning up source directory ${STORMY_GIT_DIRECTORY}"
	    rm -rf "${STORMY_GIT_DIRECTORY}"
	fi
}
