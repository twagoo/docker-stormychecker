#!/bin/sh

#first delete artifacts from latest run if there was any
rm -rf ${STORM_DIRECTORY}/datadir/*
rm -rf ${ZOOKEEPER_DIRECTORY}/datadir/*

#create screen session and set ouptut
screen -S zookeeper -dm -L -Logfile /var/log/stormychecker/zookeeper.out
#then send command to session
screen -S zookeeper -p 0 -X stuff "${ZOOKEEPER_DIRECTORY}/bin/zkServer.sh start"`echo -ne '\015'`
echo started zookeeper, waiting 3 seconds
sleep 3

#same for the rest...
screen -S nimbus -dm -L -Logfile /var/log/stormychecker/nimbus.out
screen -S nimbus -p 0 -X stuff "${STORM_DIRECTORY}/bin/storm nimbus"`echo -ne '\015'`
echo started nimbus, waiting 3 seconds
sleep 3

screen -S supervisor -dm -L -Logfile /var/log/stormychecker/supervisor.out
screen -S supervisor -p 0 -X stuff "${STORM_DIRECTORY}/bin/storm supervisor"`echo -ne '\015'`
echo started supervisor, waiting 3 second
sleep 3

screen -S stormychecker -dm -L -Logfile /var/log/stormychecker/stormychecker.out
screen -S stormychecker -p 0 -X stuff "${STORM_DIRECTORY}/bin/storm jar ${STORMY_CHECKER_DIRECTORY}/stormychecker.jar org.apache.storm.flux.Flux ${STORMY_FLUX_FILE} -remote"`echo -ne '\015'`